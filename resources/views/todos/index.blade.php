@extends('layouts.app')
@section('content')
<h1>This is your todo list</h1>

<ul> 
    @foreach($todos as $todo)
    @if ($todo->status)
           <input type = 'checkbox' id ="{{$todo->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$todo->id}}">
       @endif

    <li>
     title:<a href= "{{route('todos.edit', $todo->id )}}"> {{$todo->title}}</a>
    </li>
    @endforeach
</ul>
<a href="{{route('todos.create')}}">Create New todo </a>
@endsection