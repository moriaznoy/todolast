<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
        [
            'name' => 'Sahar',
            'email' => 's@gmail.com',
            'password' => '1234',
            'created_at' => date('Y-m-d G:i:s'),
	    ],
	    [
            'name' => 'David',
            'email' => 'd@gmail.com',
            'password' => '1234',
            'created_at' => date('Y-m-d G:i:s'),
	    ],
	    [
            'name' => 'Sima',
            'email' => 'si@gmail.com',
            'password' => '1234',
            'created_at' => date('Y-m-d G:i:s'),
	    ],

        ]);
    }
    
}
